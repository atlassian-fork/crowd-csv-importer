package com.atlassian.crowd.csvimporter;

import java.io.File;

import com.google.common.base.Optional;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.csvimporter.AppExitCode.ARGUMENTS_UNPARSIBLE;
import static com.atlassian.crowd.csvimporter.AppExitCode.GROUPS_FILE_NOT_EXIST;
import static com.atlassian.crowd.csvimporter.AppExitCode.REQUIRED_ARGUMENTS_MISSING;
import static com.atlassian.crowd.csvimporter.AppExitCode.SUCCESS;
import static com.atlassian.crowd.csvimporter.AppExitCode.USERS_FILE_NOT_EXIST;

public class AppOptionParser
{
    private static final Logger log = LoggerFactory.getLogger(AppOptionParser.class);

    private static void exitWithHelp(AppExitCode exitCode, Options options)
    {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(120);
        formatter.printHelp("crowd-csv-importer", options);
        exitCode.exit();
    }

    public static ProgramOptions parseOptions(String[] args)
    {
        final Options options = new Options()
            .addOption("h", "help", false, "Display this help message.")
            .addOption("i", "instance", true, "The name of the instance. (Required). Example: sdog.jira.com")
            .addOption("a", "application", true, "The name of the application to use. Default: maintainer")
            .addOption("p", "password", true, "The Crowd application password. (Required)")
            .addOption("n", "threads", true, "The number of concurrent HTTP requests. Default: 20")
            .addOption("g", "groups", true,
                "The path to the location of the groups file from the $CWD.")
            .addOption("u", "users", true, "The location of the users file from the $CWD.")
            .addOption("c", "context", true, "The context path of the crowd server on the instance. Default: /crowd")
            .addOption("e", "encrypted-passwords", false,
                "Use this flag if the passwords that you are providing in the users CSV are encrypted. Default: plaintext passwords expected")
            .addOption("b", "disable-blacklist", false, "Turn off blacklisting of the following groups: " + App.BLACKLISTED_GROUPS)
            .addOption("s", "https", false, "Use the https protocol. Default: http");

        // If no arguments then show help
        if (args.length == 0)
        {
            exitWithHelp(SUCCESS, options);
        }

        final CommandLineParser cli = new BasicParser();
        CommandLine parsedArguments = null;
        try
        {
            parsedArguments = cli.parse(options, args);
        }
        catch (ParseException e)
        {
            log.error("Could not parse the command line arguments.", e);
        }

        final ProgramOptions programOptions = new ProgramOptions();
        if (parsedArguments == null)
        {
            exitWithHelp(ARGUMENTS_UNPARSIBLE, options);
        }
        else
        {
            if (parsedArguments.hasOption("help"))
            {
                exitWithHelp(SUCCESS, options);
            }

            if (!parsedArguments.hasOption("instance") || !parsedArguments.hasOption("password"))
            {
                exitWithHelp(REQUIRED_ARGUMENTS_MISSING, options);
            }
            else
            {
                programOptions.setCrowdDetails(new CrowdDetails(parsedArguments.getOptionValue("instance"),
                    parsedArguments.getOptionValue("application"), parsedArguments.getOptionValue("password"),
                    parsedArguments.getOptionValue("context"), parsedArguments.hasOption("https")));
            }

            programOptions.setUsingEncryptedPasswords(parsedArguments.hasOption("encrypted-passwords"));
            programOptions.setBlacklistEnabled(!parsedArguments.hasOption("disable-blacklist"));

            if (parsedArguments.hasOption("threads"))
            {
                programOptions.setMaxHttpThreads(Integer.parseInt(parsedArguments.getOptionValue("threads")));
            }

            if (parsedArguments.hasOption("groups"))
            {
                programOptions.setGroupFileName(new File(parsedArguments.getOptionValue("groups")));
            }

            if (parsedArguments.hasOption("users"))
            {
                programOptions.setUsersFileName(new File(parsedArguments.getOptionValue("users")));
            }

            final Optional<File> potentialUsersFilename = programOptions.getUsersFileName();
            if (potentialUsersFilename.isPresent() && !potentialUsersFilename.get().canRead())
            {
                log.error("Aborting due to missing expected user file: {}", potentialUsersFilename.get());
                USERS_FILE_NOT_EXIST.exit();
            }

            final Optional<File> potentialGroupMappingFilename = programOptions.getGroupFileName();
            if (potentialGroupMappingFilename.isPresent() && !potentialGroupMappingFilename.get().canRead())
            {
                log.error("Aborting due to missing expected groups file: {}", potentialGroupMappingFilename.get());
                GROUPS_FILE_NOT_EXIST.exit();
            }
        }

        return programOptions;
    }

}

package com.atlassian.crowd.csvimporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.integration.rest.entity.UserEntity;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.crowd.csvimporter.callables.AddUserToGroupCallable;
import com.atlassian.crowd.csvimporter.callables.CreateGroupCallable;
import com.atlassian.crowd.csvimporter.callables.CreateUserCallable;
import com.atlassian.crowd.csvimporter.callables.GroupNotExistsCallable;
import com.atlassian.crowd.csvimporter.parsers.CsvMappingParser;
import com.atlassian.crowd.csvimporter.parsers.GroupMapper;
import com.atlassian.crowd.csvimporter.parsers.UserMapper;
import com.atlassian.crowd.csvimporter.parsers.entities.GroupMembership;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Optional.presentInstances;

public class App
{
    private static final Logger log = LoggerFactory.getLogger(App.class);

    @VisibleForTesting
    static final Set<String> BLACKLISTED_GROUPS = ImmutableSet.of("system-administrators",
        "confluence-administrators");

    protected static AppExitCode handleUsersFile(final ProgramOptions programOptions, final CrowdClient crowdClient,
                                                 ExecutorService executorService,
                                                 CsvMappingParser<UserEntity> userParser,
                                                 EndResults endResults)
    {
        final Optional<File> potentialUsersFilename = programOptions.getUsersFileName();
        if (potentialUsersFilename.isPresent())
        {
            final File usersFilename = potentialUsersFilename.get();
            if (usersFilename.canRead())
            {
                // Parse those files into structures that make sense
                List<UserEntity> users = null;
                try
                {
                    users = userParser.parse(usersFilename);
                }
                catch (FileNotFoundException e)
                {
                    log.error("Could not find the users file.", e);
                    AppExitCode.USERS_FILE_NOT_EXIST.exit();
                }
                log.info("Parsed {} users from the users CSV file: {}", users.size(), usersFilename);

                // Create the users that need to exist.
                endResults.setTotalUsers(users.size());
                endResults.setUsersCreated(IterableUtils.countTrue(IterableUtils.flushAll(IterableUtils.submitAll(
                    executorService, Iterables.transform(users,
                    new Function<UserEntity, CreateUserCallable>()
                    {
                        @Override
                        public CreateUserCallable apply(UserEntity user)
                        {
                            return new CreateUserCallable(crowdClient, user,
                                programOptions.isUsingEncryptedPasswords());
                        }
                    })))));
            }
            else
            {
                log.error("Could not read users file. Aborting.");
                return AppExitCode.USERS_FILE_NOT_EXIST;
            }
        }
        else
        {
            log.warn("No users file was provided. Skipping user creation.");
        }

        return AppExitCode.SUCCESS;
    }

    protected static AppExitCode handleGroupsFile(final ProgramOptions programOptions, final CrowdClient crowdClient,
                                                  ExecutorService executorService,
                                                  CsvMappingParser<GroupMembership> groupParser, EndResults endResults)
    {
        final Optional<File> potentialGroupFileName = programOptions.getGroupFileName();
        if (potentialGroupFileName.isPresent())
        {
            final File groupFileName = potentialGroupFileName.get();
            if (groupFileName.canRead())
            {
                // Parse the Group Memberships
                List<GroupMembership> groupMemberships = null;
                try
                {
                    groupMemberships = groupParser.parse(groupFileName);
                }
                catch (FileNotFoundException e)
                {
                    log.error("Could not find the group mapping file.", e);
                    AppExitCode.GROUPS_FILE_NOT_EXIST.exit();
                }
                endResults.setTotalParsedGroupMemberships(groupMemberships.size());
                log.info("Parsed {} group memberships from the groups CSV file: {}",
                    endResults.getTotalParsedGroupMemberships(), groupFileName);

                // Extract group mappings that are illegal
                final Iterable<GroupMembership> filteredGroupMemberships = programOptions.isBlacklistEnabled() ?
                    removeAdministratorGroups(groupMemberships) : groupMemberships;

                // Work out which groups are listed
                final Set<String> uniqueGroupNames = getUniqueGroupNames(filteredGroupMemberships);
                log.debug("These are the unique groups in the group mapping file: {}", uniqueGroupNames.toString());

                // Find which groups don't exist
                final Iterable<String> missingGroups = presentInstances(IterableUtils.flushAll(IterableUtils.submitAll(
                    executorService,
                    Iterables.transform(uniqueGroupNames, new Function<String, GroupNotExistsCallable>()
                    {
                        @Override
                        public GroupNotExistsCallable apply(String groupName)
                        {
                            return new GroupNotExistsCallable(crowdClient, groupName);
                        }

                    }))));
                endResults.setTotalGroups(Iterables.size(missingGroups));

                // If there are any missing groups then, create the groups that don't exist
                if (!Iterables.isEmpty(missingGroups))
                {
                    log.info("These groups do not currently exist in crowd: {}", Iterables.toString(missingGroups));

                    endResults.setGroupsCreated(IterableUtils.countTrue(IterableUtils.flushAll(IterableUtils.submitAll(
                        executorService, Iterables.transform(
                        missingGroups, new Function<String, CreateGroupCallable>()
                    {
                        @Override
                        public CreateGroupCallable apply(String missingGroup)
                        {
                            return new CreateGroupCallable(crowdClient, missingGroup);
                        }
                    })))));
                }

                // Create the Group Mappings that need to exist
                endResults.setTotalGroupMemberships(Iterables.size(filteredGroupMemberships));
                endResults.setGroupMembershipsCreated(IterableUtils.countTrue(IterableUtils.flushAll(
                    IterableUtils.submitAll(executorService, Iterables.transform(
                        filteredGroupMemberships, new Function<GroupMembership, AddUserToGroupCallable>()
                    {
                        @Override
                        public AddUserToGroupCallable apply(GroupMembership groupMembership)
                        {
                            return new AddUserToGroupCallable(crowdClient, groupMembership);
                        }
                    })))));
            }
            else
            {
                log.error("Could not read groups file. Aborting.");
                return AppExitCode.GROUPS_FILE_NOT_EXIST;
            }
        }
        else
        {
            log.warn("No groups file provided. Skipping group and membership creation.");
        }

        return AppExitCode.SUCCESS;
    }

    private static CrowdClient generateCrowdClient(ProgramOptions programOptions)
    {
        final CrowdClientFactory crowdClientFactory = new RestCrowdClientFactory();
        final CrowdDetails crowdDetails = programOptions.getCrowdDetails();
        return crowdClientFactory.newInstance(crowdDetails.getBaseUri(), crowdDetails.getApplicationName(),
            crowdDetails.getApplicationPassword());
    }


    private static String displayEndResult(int performed, int total)
    {
        if (total == 0)
        {
            return "None";
        }

        double percentage = (double) performed / (double) total;
        if (percentage > 1)
        {
            percentage = 1;
        }
        if (percentage < 0)
        {
            percentage = 0;
        }
        percentage *= 100;

        return String.format("%d / %d (%.2f%%)", performed, total, percentage);
    }

    private static final Function<GroupMembership, String> GET_GROUP_NAME = new Function<GroupMembership, String>()
    {
        @Override
        public String apply(GroupMembership input)
        {
            return input.getGroup();
        }
    };

    private static final Predicate<String> GROUPNAME_NOT_BLACKLISTED =
        Predicates.compose(Predicates.not(Predicates.in(BLACKLISTED_GROUPS)), IdentifierUtils.TO_LOWER_CASE);

    private static final Predicate<GroupMembership> GROUP_NOT_BLACKLISTED = Predicates.compose(
        GROUPNAME_NOT_BLACKLISTED, GET_GROUP_NAME);


    private static Iterable<GroupMembership> removeAdministratorGroups(List<GroupMembership> groupMemberships)
    {
        return Iterables.filter(groupMemberships, GROUP_NOT_BLACKLISTED);
    }

    public static Set<String> getUniqueGroupNames(Iterable<GroupMembership> groupMemberships)
    {
        return Sets.newHashSet(Iterables.transform(groupMemberships, new Function<GroupMembership, String>()
        {
            @Override
            public String apply(com.atlassian.crowd.csvimporter.parsers.entities.GroupMembership groupMembership)
            {
                return groupMembership.getGroup();
            }
        }));
    }

    private static void printResults(EndResults endResults)
    {
        log.info("");
        log.info("== FINAL RESULTS ==");
        log.info("");
        log.info("Groups Created: {}", displayEndResult(endResults.getGroupsCreated(), endResults.getTotalGroups()));
        log.info("Users Created: {}", displayEndResult(endResults.getUsersCreated(), endResults.getTotalUsers()));
        log.info("Legal Group Memberships: {}", displayEndResult(endResults.getTotalLegalGroupMemberships(),
            endResults.getTotalParsedGroupMemberships()));
        log.info("Group Memberships Created: {}", displayEndResult(endResults.getGroupMembershipsCreated(),
            endResults.getTotalLegalGroupMemberships()));
        log.info("");
        log.info("===================");
    }

    private static void exitIfFailure(AppExitCode appExitCode)
    {
        if (appExitCode != AppExitCode.SUCCESS)
        {
            appExitCode.exit();
        }
    }

    public static void main(String[] args)
    {
        final ProgramOptions programOptions = AppOptionParser.parseOptions(args);
        log.debug("Command line arguments are: {}", programOptions);

        final CrowdClient crowdClient = generateCrowdClient(programOptions);
        final ExecutorService executorService = Executors.newFixedThreadPool(programOptions.getMaxHttpThreads());

        final EndResults endResults = new EndResults();
        AppExitCode appExitCode = handleUsersFile(programOptions, crowdClient, executorService,
            new CsvMappingParser<UserEntity>(new UserMapper()),
            endResults);
        exitIfFailure(appExitCode);
        appExitCode = handleGroupsFile(programOptions, crowdClient, executorService,
            new CsvMappingParser<GroupMembership>(new GroupMapper()), endResults);
        exitIfFailure(appExitCode);

        // Close my services
        crowdClient.shutdown();
        executorService.shutdown();

        // Print the final results to support
        printResults(endResults);

        appExitCode.exit();
    }

}

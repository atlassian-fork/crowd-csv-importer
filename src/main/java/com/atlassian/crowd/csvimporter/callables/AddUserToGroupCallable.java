package com.atlassian.crowd.csvimporter.callables;

import java.util.concurrent.Callable;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.csvimporter.parsers.entities.GroupMembership;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddUserToGroupCallable implements Callable<Boolean>
{
    private static final Logger log = LoggerFactory.getLogger(AddUserToGroupCallable.class);

    private final CrowdClient crowdClient;
    private final GroupMembership groupMembership;

    public AddUserToGroupCallable(CrowdClient crowdClient, GroupMembership groupMembership)
    {
        this.crowdClient = crowdClient;
        this.groupMembership = groupMembership;
    }

    @Override
    public Boolean call()
    {
        try
        {
            crowdClient.addUserToGroup(groupMembership.getUsername(), groupMembership.getGroup());
            log.info("Group Mapping Created (User => Group): {} => {}", groupMembership.getUsername(),
                groupMembership.getGroup());
            return Boolean.TRUE;
        }
        catch (MembershipAlreadyExistsException e)
        {
            log.info("Group Mapping already existed (User => Group): {} => {}", groupMembership.getUsername(),
                groupMembership.getGroup());
            return Boolean.TRUE;
        }
        catch (GroupNotFoundException e)
        {
            log.error(
                "Group Mapping for {} => {} not created. The group did not exist and it should have been created previously. Double check logs.",
                groupMembership.getUsername(), groupMembership.getGroup());
        }
        catch (UserNotFoundException e)
        {
            log.error(
                "The user {} does not exist so it could not be added to the group {}.",
                groupMembership.getUsername(), groupMembership.getGroup());
        }
        catch (OperationFailedException | ApplicationPermissionException | InvalidAuthenticationException e)
        {
            log.error("Could not add {} to group {} because of unexpected response from rest API.",
                groupMembership.getUsername(), groupMembership.getGroup());
            log.error("Rest api failed with an error.", e);
        }

        return Boolean.FALSE;
    }
}

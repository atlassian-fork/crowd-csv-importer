package com.atlassian.crowd.csvimporter;

public class CrowdDetails
{
    private final String hostname, contextPath, applicationName, applicationPassword;
    private final boolean useHttps;

    public CrowdDetails(String hostname, String applicationName, String applicationPassword, String contextPath,
                        boolean useHttps)
    {
        this.hostname = hostname;
        this.applicationName = applicationName == null ? "maintainer" : applicationName;
        this.applicationPassword = applicationPassword;
        this.contextPath = contextPath == null ? "/crowd" : contextPath;
        this.useHttps = useHttps;
    }

    public String getBaseUri()
    {
        final String trueContextPath = this.contextPath.startsWith("/") ? this.contextPath : "/" + this.contextPath;
        return getProtocol() + hostname + trueContextPath;
    }

    public String getHostname()
    {
        return hostname;
    }

    public String getContextPath()
    {
        return contextPath;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public String getApplicationPassword()
    {
        return applicationPassword;
    }

    public String getProtocol()
    {
        if (useHttps) return "https://";
        return "http://";
    }
}
